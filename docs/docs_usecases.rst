.. This work is licensed under a Creative Commons Attribution 4.0
   International License. http://creativecommons.org/licenses/by/4.0
   Copyright 2018 Huawei Technologies Co., Ltd.  All rights reserved.

.. _docs_usecases:

Verified Use Cases and Functional Requirements
----------------------------------------------

Description
~~~~~~~~~~~
This session includes use cases and functional requirements have been verified in Casablanca release by the Integration team:
    1. What has been implemented
    2. Step by step instruction on how to deploy them, including the links to download the related assets and resources 
    3. Known issues and workaround
    
The final testing status can be found at `Casablanca Release Integration Testing Status Summery <https://wiki.onap.org/display/DW/Casablanca+Release+Integration+Testing+Status>`_

Use Cases
~~~~~~~~~
:ref:`vFirewall Use Case <docs_vfw>`

:ref:`vLoadBalancer Use Case <docs_vlb>`

:ref:`vCPE Use Case <docs_vcpe>`

:ref:`vCPE with TOSCA VNF (w/ HPA) Use Case <docs_vcpe_tosca>`

:ref:`VF Module Scale Out Use Case <docs_scaleout>`

:ref:`CCVPN (Cross Domain and Cross Layer VPN) Use Case <docs_ccvpn>`

:ref:`vFirewall with HPA Use Case <docs_vfw_hpa>`

:ref:`vFirewall Traffic Distribution Use Case <docs_vfw_traffic>`


Functional Requirements
~~~~~~~~~~~~~~~~~~~~~~~
:ref:`5G - Real Time PM and High Valume Stream Data Collection <docs_realtime_pm>`

:ref:`5G - PNF Plug and Play <docs_5g_pnf_pnp>`

:ref:`5G - Buld PM <docs_5g_bulk_pm>`

:ref:`5G - PNF Software Upgrade <docs_5g_pnf_software_upgrade>`

:ref:`5G - OOF and PCI <docs_5G_oof_pci>`

:ref:`Change Management Flexible Designer and Orchestrator <docs_CM_flexible_designer_orchestrator>`

:ref:`Change Management Schedule Optimization <docs_CM_schedule_optimizer>`

